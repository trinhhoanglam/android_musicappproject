package com.group7.musicappproject.Activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.group7.musicappproject.Adapter.ViewPagerPlayNhac;
import com.group7.musicappproject.Fragment.Fragment_Dia_Nhac;
import com.group7.musicappproject.Fragment.Fragment_Lyric;
import com.group7.musicappproject.Fragment.Fragment_Play_Danh_Sach_Bai_Hat;
import com.group7.musicappproject.Interface.MainCallbacks;
import com.group7.musicappproject.Model.BaiHat;
import com.group7.musicappproject.R;
import com.group7.musicappproject.Service.Constants;
import com.group7.musicappproject.Service.PlayService;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Random;

public class PlayActivity extends AppCompatActivity implements MainCallbacks {

    Toolbar toolbar;
    TextView txtTime, txtTotalTime;
    SeekBar seekBarTime;
    ImageButton btnPlay, btnNext, btnPre, btnRepeat, btnRandom, btnTimer;
    ViewPager viewPagerPlay;
    public static ArrayList<BaiHat> baiHatArrayList = new ArrayList<>();
    public static ViewPagerPlayNhac adapterNhac;
    Fragment_Dia_Nhac fragment_dia_nhac;
    Fragment_Play_Danh_Sach_Bai_Hat fragment_play_danh_sach_bai_hat;
    Fragment_Lyric fragment_lyric;

    MediaPlayer mediaPlayer;
    boolean isLocal = false;
    int position = 0;
    boolean repeat = false;
    boolean checkRandom = false;
    boolean next = false;

    BroadcastReceiver receiver;

    Intent intent;

    int selectTimer = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        intent = new Intent(PlayActivity.this, PlayService.class);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        getData();
        map();
        event_click();

        IntentFilter filter_play  = new IntentFilter(Constants.ACTION.MYPLAY_ACTION);
        IntentFilter filter_next = new IntentFilter(Constants.ACTION.MYNEXT_ACTION);
        IntentFilter filter_pre = new IntentFilter(Constants.ACTION.MYPRE_ACTION);

        receiver = new MyEmbeddedBroadcastReceiver();
        registerReceiver(receiver, filter_play);
        registerReceiver(receiver, filter_next);
        registerReceiver(receiver, filter_pre);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mediaPlayer.stop();
        mediaPlayer.release();
        mediaPlayer = null;
        stopService(intent);
    }

    private void event_click() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (adapterNhac.getItem(1) != null) {
                    if (baiHatArrayList.size() > 0) {
                        fragment_dia_nhac.setImg(baiHatArrayList.get(position).getHinhBaiHat(), isLocal);
                        fragment_lyric.setTxtLyric(getLyric(baiHatArrayList.get(position).getLinkBaiHat()));
                        handler.removeCallbacks(this);
                    } else {
                        handler.postDelayed(this, 500);
                    }
                }
            }
        }, 500);
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.setAction(Constants.ACTION.ACTPLAY_ACTION);
                stopService(intent);
                startService(intent);
                onBtnPlay();
            }
        });
        btnRepeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!repeat) {
                    if (checkRandom){
                        checkRandom = !checkRandom;
                        btnRandom.setImageResource(R.drawable.iconsuffle);
                    }
                    btnRepeat.setImageResource(R.drawable.iconsyned);
                    repeat = !repeat;
                } else {
                   btnRepeat.setImageResource(R.drawable.iconrepeat);
                   repeat = !repeat;
                }
            }
        });
        btnRandom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkRandom) {
                    if (repeat){
                        repeat = !repeat;
                        btnRepeat.setImageResource(R.drawable.iconrepeat);
                    }
                    btnRandom.setImageResource(R.drawable.iconshuffled);
                    checkRandom = !checkRandom;
                } else {
                    btnRandom.setImageResource(R.drawable.iconsuffle);
                    checkRandom = !checkRandom;
                }
            }
        });
        seekBarTime.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mediaPlayer.seekTo(seekBar.getProgress());
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBtnNext();
            }
        });
        btnPre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBtnPre();
            }
        });
    }

    private void onBtnPlay() {
        intent.putExtra("songName", baiHatArrayList.get(position).getTenBaiHat());
        intent.putExtra("artist", baiHatArrayList.get(position).getCaSi());
        if (mediaPlayer.isPlaying()){
            mediaPlayer.pause();
            intent.setAction(Constants.ACTION.ACTPLAY_ACTION);
            btnPlay.setImageResource(R.drawable.iconplay);
        } else {
            intent.setAction(Constants.ACTION.MAIN_ACTION);
            mediaPlayer.start();
            btnPlay.setImageResource(R.drawable.iconpause);
        }
        startService(intent);
    }

    private void onBtnNext() {
        if (baiHatArrayList.size() > 0) {
            if (mediaPlayer.isPlaying() || mediaPlayer != null){
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
            }
            if (position < baiHatArrayList.size()){
                btnPlay.setImageResource(R.drawable.iconpause);
                position++;
                if (repeat) {
                    if (position == 0){
                        position = baiHatArrayList.size();
                    }
                    position -= 1;
                }
                if (checkRandom) {
                    Random random = new Random();
                    int index = random.nextInt(baiHatArrayList.size());
                    if (index == position) {
                        position = index - 1;
                    }
                    position = index;
                }
                if (position > baiHatArrayList.size() - 1){
                    position = 0;
                }
                new PlayMp3().execute(baiHatArrayList.get(position).getLinkBaiHat());
                fragment_dia_nhac.setImg(baiHatArrayList.get(position).getHinhBaiHat(), isLocal);
                fragment_lyric.setTxtLyric(getLyric(baiHatArrayList.get(position).getLinkBaiHat()));
                getSupportActionBar().setTitle(baiHatArrayList.get(position).getTenBaiHat());
                updateTime();
                setNotification();
            }
        }
        btnPre.setClickable(false);
        btnNext.setClickable(false);
        final Handler handler1 = new Handler();
        handler1.postDelayed(new Runnable() {
            @Override
            public void run() {
                btnPre.setClickable(true);
                btnNext.setClickable(true);
                handler1.removeCallbacks(this);
            }
        }, 3000);
    }

    private void onBtnPre() {
        if (baiHatArrayList.size() > 0) {
            if (mediaPlayer.isPlaying() || mediaPlayer != null){
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
            }
            if (position < baiHatArrayList.size()){
                btnPlay.setImageResource(R.drawable.iconpause);
                position--;
                if (position < 0) {
                    position = baiHatArrayList.size() - 1;
                }
                if (repeat) {
                    position += 1;
                }
                if (checkRandom) {
                    Random random = new Random();
                    int index = random.nextInt(baiHatArrayList.size());
                    if (index == position) {
                        position = index - 1;
                    }
                    position = index;
                }
                new PlayMp3().execute(baiHatArrayList.get(position).getLinkBaiHat());
                fragment_dia_nhac.setImg(baiHatArrayList.get(position).getHinhBaiHat(), isLocal);
                fragment_lyric.setTxtLyric(getLyric(baiHatArrayList.get(position).getLinkBaiHat()));
                getSupportActionBar().setTitle(baiHatArrayList.get(position).getTenBaiHat());
                updateTime();
                setNotification();
            }
        }
        btnPre.setClickable(false);
        btnNext.setClickable(false);
        final Handler handler1 = new Handler();
        handler1.postDelayed(new Runnable() {
            @Override
            public void run() {
                btnPre.setClickable(true);
                btnNext.setClickable(true);
                handler1.removeCallbacks(this);
            }
        }, 3000);
    }

    private void getData() {
        Intent intent = getIntent();
        baiHatArrayList.clear();
        if (intent == null) return;
        if (intent.hasExtra("baiHat")) {
            BaiHat baiHat = intent.getParcelableExtra("baiHat");
            baiHatArrayList.add(baiHat);
        }
        if (intent.hasExtra("arrayBaiHat")) {
            ArrayList<BaiHat> _baiHatArrayList = intent.getParcelableArrayListExtra("arrayBaiHat");
            baiHatArrayList = _baiHatArrayList;
        }
        if (intent.hasExtra("nhacCaNhan")) {
            ArrayList<BaiHat> baiHats = intent.getParcelableArrayListExtra("nhacCaNhan");
            baiHatArrayList = baiHats;
            position = intent.getIntExtra("pos", 0);
            isLocal = true;
        }
    }

    private void map() {
        toolbar = findViewById(R.id.toolBarPlayNhac);
        txtTime = findViewById(R.id.txtTime);
        txtTotalTime = findViewById(R.id.txtTotalTime);
        seekBarTime = findViewById(R.id.seekBarSong);
        btnPlay = findViewById(R.id.btnPlay);
        btnNext = findViewById(R.id.btnNext);
        btnPre = findViewById(R.id.btnPrevious);
        btnRandom = findViewById(R.id.btnSuffle);
        btnRepeat = findViewById(R.id.btnRepeat);
        btnTimer = findViewById(R.id.btnTimer);
        viewPagerPlay = findViewById(R.id.viewpagerPlay);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                mediaPlayer.stop();
                baiHatArrayList.clear();
            }
        });
        toolbar.setTitleTextColor(Color.WHITE);
        btnTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogTime();
            }
        });
        adapterNhac = new ViewPagerPlayNhac(getSupportFragmentManager());
        fragment_play_danh_sach_bai_hat = new Fragment_Play_Danh_Sach_Bai_Hat();
        adapterNhac.addFragment(fragment_play_danh_sach_bai_hat);
        fragment_dia_nhac = new Fragment_Dia_Nhac();
        adapterNhac.addFragment(fragment_dia_nhac);
        fragment_lyric = new Fragment_Lyric();
        adapterNhac.addFragment(fragment_lyric);
        viewPagerPlay.setAdapter(adapterNhac);
        viewPagerPlay.setCurrentItem(1);
        fragment_dia_nhac = (Fragment_Dia_Nhac) adapterNhac.getItem(1);
        fragment_lyric = (Fragment_Lyric) adapterNhac.getItem(2);
        if (baiHatArrayList.size() > 0) {
            getSupportActionBar().setTitle(baiHatArrayList.get(position).getTenBaiHat());
            new PlayMp3().execute(baiHatArrayList.get(position).getLinkBaiHat());
            btnPlay.setImageResource(R.drawable.iconpause);
            intent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
            intent.putExtra("songName", baiHatArrayList.get(position).getTenBaiHat());
            intent.putExtra("artist", baiHatArrayList.get(position).getCaSi());
            startService(intent);
        }
    }

    private void showDialogTime() {
        AlertDialog.Builder dialog;
        final String [] arr ={"1", "3","30","90"};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            dialog  = new AlertDialog.Builder(this);
        }
        dialog.setTitle("Hẹn giờ")
                .setSingleChoiceItems(arr, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectTimer = Integer.parseInt(arr[which]);
                        //Toast.makeText(getApplication(), selectTimer, Toast.LENGTH_SHORT).show();

                    }
                })
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (selectTimer != -1) {
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    finish();
                                }
                            }, selectTimer * 60000);
                        }
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })

                .show();
    }

    private void setNotification() {
        intent.setAction(Constants.ACTION.MAIN_ACTION);
        intent.putExtra("songName", baiHatArrayList.get(position).getTenBaiHat());
        intent.putExtra("artist", baiHatArrayList.get(position).getCaSi());
        startService(intent);
    }

    @Override
    public void onMsgFromFragToMain(String sender, int pos) {
        if (sender.equals("LIST-SONG")) {
            position = pos;
            if (mediaPlayer.isPlaying() || mediaPlayer != null){
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
            }
            new PlayMp3().execute(baiHatArrayList.get(position).getLinkBaiHat());
            fragment_dia_nhac.setImg(baiHatArrayList.get(position).getHinhBaiHat(), isLocal);
            fragment_lyric.setTxtLyric(getLyric(baiHatArrayList.get(position).getLinkBaiHat()));
            getSupportActionBar().setTitle(baiHatArrayList.get(position).getTenBaiHat());
            updateTime();
            setNotification();
        }
    }

    public class PlayMp3 extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            return strings[0];
        }

        @Override
        protected void onPostExecute(String baiHat) {
            super.onPostExecute(baiHat);
            try {
                if (isLocal) {
                    Uri u = Uri.parse(baiHatArrayList.get(position).getLinkBaiHat());
                    mediaPlayer = MediaPlayer.create(PlayActivity.this, u);
                } else {
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mediaPlayer.setDataSource(baiHat);
                    mediaPlayer.prepare();
                }
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mediaPlayer.stop();
                        mediaPlayer.reset();
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
            mediaPlayer.start();
            TimeSong();
            updateTime();
        }
    }

    private void TimeSong() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm:ss");
        txtTotalTime.setText(simpleDateFormat.format(mediaPlayer.getDuration()));
        seekBarTime.setMax(mediaPlayer.getDuration());
    }

    private  void updateTime() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mediaPlayer != null){
                    seekBarTime.setProgress(mediaPlayer.getCurrentPosition());
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mm:ss");
                    txtTime.setText(simpleDateFormat.format(mediaPlayer.getCurrentPosition()));
                    handler.postDelayed(this, 300);
                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            next = true;
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        }, 300);
        final Handler handler1 = new Handler();
        handler1.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (next){
                    if (baiHatArrayList.size() > 0) {
                        if (mediaPlayer.isPlaying() || mediaPlayer != null){
                            mediaPlayer.stop();
                            mediaPlayer.release();
                            mediaPlayer = null;
                        }
                        if (position < baiHatArrayList.size()){
                            btnPlay.setImageResource(R.drawable.iconpause);
                            position++;
                            if (repeat) {
                                if (position == 0){
                                    position = baiHatArrayList.size();
                                }
                                position -= 1;
                            }
                            if (checkRandom) {
                                Random random = new Random();
                                int index = random.nextInt(baiHatArrayList.size());
                                if (index == position) {
                                    position = index - 1;
                                }
                                position = index;
                            }
                            if (position > baiHatArrayList.size() - 1){
                                position = 0;
                            }
                            new PlayMp3().execute(baiHatArrayList.get(position).getLinkBaiHat());
                            fragment_dia_nhac.setImg(baiHatArrayList.get(position).getHinhBaiHat(), isLocal);
                            fragment_lyric.setTxtLyric(getLyric(baiHatArrayList.get(position).getLinkBaiHat()));
                            getSupportActionBar().setTitle(baiHatArrayList.get(position).getTenBaiHat());
                        }
                    }
                    btnPre.setClickable(false);
                    btnNext.setClickable(false);
                    Handler handler1 = new Handler();
                    handler1.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            btnPre.setClickable(true);
                            btnNext.setClickable(true);
                            handler.removeCallbacks(this);
                        }
                    }, 3000);
                    next = false;
                    handler1.removeCallbacks(this);
                } else {
                    handler1.postDelayed(this, 1000);
                }
            }
        }, 1000);
    }

    String getLyric(String link) {
        AudioFile audioFile = null;
        File file = null;
        String lyric = null;

        try {
            file = new File(link);
            try {
                audioFile = AudioFileIO.read(file);
            } catch (CannotReadException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (org.jaudiotagger.tag.TagException e1) {
                e1.printStackTrace();
            } catch (ReadOnlyFileException e1) {
                e1.printStackTrace();
            } catch (InvalidAudioFrameException e1) {
                e1.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Tag tag = audioFile.getTag();
            lyric = tag.getFirst(FieldKey.LYRICS);
        } catch (Exception e) {
            e.printStackTrace();
            lyric = "";
        }

        return lyric;
    }

    public class MyEmbeddedBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Constants.ACTION.MYPLAY_ACTION)) {
                onBtnPlay();
            } else if (intent.getAction().equals(Constants.ACTION.MYPRE_ACTION)) {
                onBtnPre();
            } else if (intent.getAction().equals(Constants.ACTION.MYNEXT_ACTION)) {
                onBtnNext();
            }
        }
    }
}
