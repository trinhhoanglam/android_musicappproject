package com.group7.musicappproject.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.group7.musicappproject.Activity.MainActivity;
import com.group7.musicappproject.Activity.PlayActivity;
import com.group7.musicappproject.Adapter.NhacCaNhanAdapter;
import com.group7.musicappproject.Model.BaiHat;
import com.group7.musicappproject.R;
import com.group7.musicappproject.Service.Constants;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class Fragment_nhac_ca_nhan extends Fragment {
    private int STORE_PERMISSION_CODE = 1;

    View view;
    CoordinatorLayout coordinatorLayout;
    CollapsingToolbarLayout collapsingToolbarLayout;
    Toolbar toolbar;
    RecyclerView recyclerView;
    FloatingActionButton floatingActionButton;
    Button btnChooseDir;
    NhacCaNhanAdapter adapter;
    ArrayList<BaiHat> baiHatArrayList = new ArrayList<>();

    Dialog dialog;
    Button btnOK;
    Button btnUP;
    TextView txtFolder;
    ListView lvItems;

    File root, curFolder;

    private List<String> fileList = new ArrayList<>();
    private List<String> fileNameList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        view = inflater.inflate(R.layout.fragment_nhac_ca_nhan, container, false);
        coordinatorLayout = view.findViewById(R.id.coordinatorCaNhan);
        collapsingToolbarLayout = view.findViewById(R.id.collaptoolBarNhacCaNhan);
        toolbar = view.findViewById(R.id.toolBarNhacCaNhan);
        recyclerView = view.findViewById(R.id.recyclerViewNhacCaNhan);
        floatingActionButton = view.findViewById(R.id.floatingNhacCaNhan);
        btnChooseDir = view.findViewById(R.id.btnNhacCaNhan);
        btnChooseDir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                performFileSeach();
            }
        });
        floatingActionButton.setEnabled(false);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PlayActivity.class);
                intent.putExtra("nhacCaNhan", baiHatArrayList);
                intent.putExtra("pos", 0);
                startActivity(intent);
            }
        });

        dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.custom_dialog);
        dialog.setTitle("Chọn thư viện");
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        txtFolder = dialog.findViewById(R.id.txtFolder);
        lvItems = dialog.findViewById(R.id.lvFolder);
        btnUP = dialog.findViewById(R.id.btnUp);
        btnOK = dialog.findViewById(R.id.btnCofirm);

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                recyclerView
                dialog.dismiss();
                setListSong();
            }
        });
        btnUP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ListDir(curFolder.getParentFile());
            }
        });
        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                File selected = new File(fileList.get(position));
                if (selected.isDirectory()){
                    ListDir(selected);
                } else {
                    Log.d("BBB", "onItemClick: cannot choose file");
                }
            }
        });

        if (isGrantExternalRW(getActivity())) {

            String path = readFromFile();

            root = new File(Environment.getExternalStorageDirectory().getAbsolutePath());

            if (path.equals("")) {
                curFolder = root;
            } else {
                curFolder = new File(path);
                ListDir(curFolder);
                setListSong();
            }
        }

        return view;
    }

    public static boolean isGrantExternalRW(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && (context.checkSelfPermission(
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {

            ((Activity)context).requestPermissions(new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            }, Constants.PERMISSION_REQUEST_CODE);

            return false;
        }

        return true;
    }

    private void performFileSeach() {

        ListDir(curFolder);

        dialog.show();
    }

    public ArrayList<File> findSongs(File root) {
        ArrayList<File> allSongs = new ArrayList<File>();

        File[] files = root.listFiles();

        for (File singleFile : files) {
            if (singleFile.isDirectory() && !singleFile.isHidden()) {
                allSongs.addAll(findSongs(singleFile));
            }
            else {
                if (singleFile.getName().endsWith(".mp3") && !singleFile.isHidden()) {
                    allSongs.add(singleFile);
                }
            }
        }

        return allSongs;
    }

    private void setListSong() {
        floatingActionButton.setEnabled(true);
        final ArrayList<File> mySongs = findSongs(curFolder);
        baiHatArrayList.clear();

        for (int i = 0; i < mySongs.size(); i++) {
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            mmr.setDataSource(mySongs.get(i).toString());
            String artist = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
            if (artist == null) {
                artist = "unknown";
            }
            String songName = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
            if (songName == null) {
                songName = mySongs.get(i).getName();
            }
            baiHatArrayList.add(new BaiHat("0", songName, mySongs.get(i).toString(), artist, mySongs.get(i).toString(), "0"));
        }

        adapter = new NhacCaNhanAdapter(getActivity(), baiHatArrayList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        writeToFile(curFolder.getPath());
    }

    private void ListDir(File f) {
        if (f.equals(root)) {
            btnUP.setEnabled(false);
        } else {
            btnUP.setEnabled(true);
        }

        curFolder = f;
        txtFolder.setText(f.getPath());

        File[] files = f.listFiles();
        fileList.clear();
        fileNameList.clear();

        for (File file : files) {
            fileList.add(file.getPath());
            fileNameList.add(file.getName());
        }

        ArrayAdapter<String> adp = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, fileNameList);
        lvItems.setAdapter(adp);
    }

    private void writeToFile(String data) {
        Context context = getActivity();
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("mussicappproject.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private String readFromFile() {
        Context context = getActivity();
        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("mussicappproject.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
}
