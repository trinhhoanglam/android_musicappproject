package com.group7.musicappproject.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.group7.musicappproject.R;

public class Fragment_Lyric extends Fragment {
    View view;
    TextView txtLyric;
    String lyric;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_lyric, container, false);
        txtLyric = view.findViewById(R.id.txtLyric);

        return view;
    }

    public void setTxtLyric(String lyric) {
        this.lyric = lyric;
        txtLyric.setText(lyric);
    }

    @Override
    public void onResume() {
        super.onResume();
        txtLyric.setText(lyric);
    }
}
