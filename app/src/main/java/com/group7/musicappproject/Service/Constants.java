package com.group7.musicappproject.Service;

public class Constants {
    public interface ACTION{
        public static String MYPLAY_ACTION ="com.group7.musicappproject.Service.myplay";
        public static String MYPRE_ACTION ="com.group7.musicappproject.Service.mypre";
        public static String MYNEXT_ACTION ="com.group7.musicappproject.Service.mynext";
        public static String ACTPLAY_ACTION ="com.group7.musicappproject.Service.actplay";
        public static String MAIN_ACTION ="com.group7.musicappproject.Service.main";
        public static String PREV_ACTION ="com.group7.musicappproject.Service.prev";
        public static String PLAY_ACTION ="com.group7.musicappproject.Service.play";
        public static String NEXT_ACTION="com.group7.musicappproject.Service.next";
        public static String STARTFOREGROUND_ACTION  ="com.group7.musicappproject.Service.startforeground";
        public static String STOPFOREGROUND_ACTION  ="com.group7.musicappproject.Service.stopforeground";
    }
    public static int PERMISSION_REQUEST_CODE = 1;
    public interface NOTIFICATION_ID{
        public static int FOREGROUND_SERVICE = 123;
    }
}
