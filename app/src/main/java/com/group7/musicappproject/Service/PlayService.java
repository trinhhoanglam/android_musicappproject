package com.group7.musicappproject.Service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.group7.musicappproject.Activity.PlayActivity;
import com.group7.musicappproject.R;

public class PlayService extends Service {

    private static final String TAG = "MyService";

    private static boolean mServiceRunning = false;

    private final MyBinder mMyBinder = new MyBinder();

    // MediaPlayer player;
    private static final String LOG_TAG ="ForegroundService";
    private int isPlay= android.R.drawable.ic_media_play;
    private String _isPlay = "||";
    private boolean isClick = false;
    private boolean isClickFromActivity = false;
    private String songName = "Unknown";
    private String artist = "Unknown";

    private BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onReceive(Context context, Intent intent) {
            receiverAction(intent);
        }
    };
    @RequiresApi(api = Build.VERSION_CODES.O)
    public void receiverAction(Intent intent)
    {
        Intent intent1 = new Intent();

        songName = intent.getStringExtra("songName");
        artist = intent.getStringExtra("artist");

        if(intent.getAction().equals(Constants.ACTION.PREV_ACTION) || intent.getAction().equals(Constants.ACTION.NEXT_ACTION)){
            intent1.setAction(Constants.ACTION.MYPRE_ACTION);
            isPlay = android.R.drawable.ic_media_play;
            _isPlay = "||";
            isClick = true;
        }
        else if(intent.getAction().equals(Constants.ACTION.PLAY_ACTION)){
            intent1.setAction(Constants.ACTION.MYPLAY_ACTION);
            if(isPlay == android.R.drawable.ic_media_pause) {
                isPlay = android.R.drawable.ic_media_play;
                _isPlay = "||";
            }
            else {
                isPlay = android.R.drawable.ic_media_pause;
                _isPlay = "▶";
            }
            isClick = true;
        } else if (intent.getAction().equals(Constants.ACTION.ACTPLAY_ACTION)) {
            isPlay = android.R.drawable.ic_media_pause;
            _isPlay = "▶";
        } else if (intent.getAction().equals(Constants.ACTION.MAIN_ACTION)) {
            isPlay = android.R.drawable.ic_media_play;
            _isPlay = "||";
        }

        if (isClick) {
            sendBroadcast(intent1);
            isClick = false;
        }

        //notification
        //Main intent
        //Toast.makeText(this,"add", Toast.LENGTH_SHORT).show();
        Intent notifi = new Intent(this, PlayActivity.class);
        //intent.setAction(Constants.ACTION.MAIN_ACTION); //Set action chung duoc thuc hien
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); //dat co kiem soat intent

        //1 PendingIntent la 1 token dua den 1 ung dung khac(vi du notification manager, alarm), cho phep
        //ung dung khac nay su dung permission trong ung dung cua ban de thuc hien 1 chuc nang nao do
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0,
                notifi, 0);
        //Previous
        Intent previousIntent = new Intent(this, PlayService.class);
        previousIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        previousIntent.setAction(Constants.ACTION.PREV_ACTION);
        PendingIntent ppreviousIntent = PendingIntent.getService(this, 0,
                previousIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        //play
        Intent playIntent = new Intent(this, PlayService.class);
        playIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        playIntent.setAction(Constants.ACTION.PLAY_ACTION);
        PendingIntent pplayIntent = PendingIntent.getService(this, 0,
                playIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        //next
        Intent nextIntent = new Intent(this, PlayService.class);
        nextIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        nextIntent.setAction(Constants.ACTION.NEXT_ACTION);
        PendingIntent pnextIntent = PendingIntent.getService(this, 0,
                nextIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.iconloved);

        String NOTIFICATION_CHANNEL_ID = "com.example.simpleapp";
        String channelName = "My Background Service";
        NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        Notification notification = notificationBuilder
                .setOngoing(true)
                .setContentTitle(songName)
                .setTicker("Music Player")
                .setContentText(artist)
                .setSmallIcon(R.drawable.iconloved)
                .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setPriority(Notification.PRIORITY_MAX)
                .setWhen(0)
                .setContentIntent(pendingIntent)
                .addAction(android.R.drawable.ic_media_previous, "<",ppreviousIntent)
                .addAction(isPlay, _isPlay,pplayIntent)
                .addAction(android.R.drawable.ic_media_next, ">", pnextIntent)
                .build();
        startForeground(Constants.NOTIFICATION_ID.FOREGROUND_SERVICE, notification);

    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        super.onCreate();
        IntentFilter intentFilter= new IntentFilter();
        intentFilter.addAction(Constants.ACTION.PREV_ACTION);
        intentFilter.addAction(Constants.ACTION.PLAY_ACTION);
        intentFilter.addAction(Constants.ACTION.NEXT_ACTION);
        registerReceiver(mIntentReceiver,intentFilter);
        mServiceRunning = true;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mMyBinder;
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    //duoc goi khi o main activity goi startservice
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Log.d(TAG, "aaaaaaaaaaaaaaaa");

        receiverAction(intent);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopForeground(true);
        mServiceRunning = false;
        unregisterReceiver(mIntentReceiver);
    }

    // Tao 1 cai Binder, cai nay can thi service moi bind dc
    public class MyBinder extends Binder {
        public PlayService getService() {
            return PlayService.this;
        }
    }
}
